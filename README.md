# Eurofurence Help Center

* Frequently Asked Questions (utilizes SQL Fulltext Search)
* Contact Form (utilized system mail())
* Legal Stuff such as Imprint, Attributions and License


## Requirements
* HTTP Server with mod_rewrite
* PHP 5.6+ with mbstring, intl, simplexml
* MySQL 5.1.10+


## Installation

1. Create a clear CakePHP skeleton by following  [CakePHP's Installation Instructions](https://book.cakephp.org/3.0/en/installation.html)

2. Clone this repository onto the skeleton.

3. Run `.setup/setup.sql` for table creation and test data.


## Update

Updates are to be tested carefully! In order to update the CakePHP base, follow [CakePHP's instructions on update](https://book.cakephp.org/3.0/en/installation.html#keeping-up-to-date-with-the-latest-cakephp-changes).
To update UIkit, the front-end framework, download the newest build of Version 3 from [UIkit's website](http://getuikit.com/) and replace the according files:

 * `/webroot/css/uikit.min.css` 
 * `/webroot/js/uikit.min.js`
 * `/webroot/js/uikit-icons.min.js`


## Configuration

Copy `config/app.default.php` to `config/app.php` and fill out or alter all relevant settings:

 * `'Debug'`
 * `'GoogleReCaptcha'`
 * `'Security / salt'`
 * `'Datasources / default'`


## Layout

For UI development, consult [UIkit's documentation](https://getuikit.com/docs/).
