DROP TABLE IF EXISTS departments_users;
DROP TABLE IF EXISTS texts;
DROP TABLE IF EXISTS articles;
DROP TABLE IF EXISTS templates;
DROP TABLE IF EXISTS departments;
DROP TABLE IF EXISTS users;

CREATE TABLE users (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    role VARCHAR(31) NOT NULL DEFAULT 'user',
	enabled BOOLEAN NOT NULL DEFAULT FALSE,
	token VARCHAR(255),
    created DATETIME NOT NULL DEFAULT NOW(),
    modified DATETIME NOT NULL DEFAULT NOW()
);

CREATE TABLE departments (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	reference VARCHAR(255),
	email VARCHAR(255),
	hint TEXT,
	created DATETIME NOT NULL DEFAULT NOW(),
	modified DATETIME NOT NULL DEFAULT NOW()
);

CREATE TABLE articles (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	department_id INT UNSIGNED,
	notes TEXT,
	question VARCHAR(1023) NOT NULL,
	answer TEXT,
	enabled BOOLEAN DEFAULT FALSE,
	created DATETIME NOT NULL DEFAULT NOW(),
	modified DATETIME NOT NULL DEFAULT NOW(),
	FOREIGN KEY (department_id) REFERENCES departments(id),
	FULLTEXT (question),
	FULLTEXT (answer),
	FULLTEXT (question, answer)
);

CREATE TABLE templates (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	department_id INT UNSIGNED,
	reference VARCHAR(63) UNIQUE,
	subject VARCHAR(255),
	message TEXT,
	created DATETIME NOT NULL DEFAULT NOW(),
	modified DATETIME NOT NULL DEFAULT NOW(),
	FOREIGN KEY (department_id) REFERENCES departments(id)
);

CREATE TABLE departments_users (
	user_id INT UNSIGNED NOT NULL,
	department_id INT UNSIGNED NOT NULL,
	FOREIGN KEY (user_id) REFERENCES users(id),
	FOREIGN KEY (department_id) REFERENCES departments(id)
);

CREATE TABLE texts (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	reference VARCHAR(255) UNIQUE,
	user_id INT UNSIGNED,
	enabled BOOLEAN DEFAULT TRUE,
	notes TEXT,
	content TEXT,
	created DATETIME NOT NULL DEFAULT NOW(),
	modified DATETIME NOT NULL DEFAULT NOW(),
	FOREIGN KEY (user_id) REFERENCES users(id)
);

INSERT INTO `departments` (`name`, `reference`, `email`, `hint`) VALUES
	('Chairman', 'chairman', 'chairman@eurofurence.org','You have something very, very urgent that cannot be discussed with anyone else.'),
	('Vice Chairman', 'vice-chairman', 'vice-chairman@eurofurence.org','You have something very, very urgent that cannot be discussed either with anyone else, nor with the Chairman himself.'),
	('Accounting & Payment', 'accounting','payment@eurofurence.org','Two weeks after your payment have passed, and still you haven\'t received any confirmation
You still have questions about your payment methods after reading the guide on our registration page'),
	('Registration', 'reg', 'reg@eurofurence.org','You need to transfer your account to someone else
You want to change details of your registration'),
	('Con Operations', 'conops', 'operations@eurofurence.org', 'there\'s something for Lost and Found or you miss something 
You like to Volunteer to support Eurofurence
there are logistic questions
you have questions regarding the EF Lockerservice
you have questions regarding the EF Helpdesk'),
	('Security', 'security', 'security@eurofurence.org','Questions have remained about our policies
You want to bring along a weapon replica and need it acknowledged by Security
Your event needs special security attention
You need us to know about personal medical or psychological conditions
There is or might be a situation our security might need to know about'),
	('Programming', 'programming', 'events@eurofurence.org','You want to host an event
There are questions regarding the event schedule
You need special requirements (rooms, hardware...) for your event'),
	('Stage', 'stage', 'stage@eurofurence.org','You want to generally apply to that team
Have a stage-related event idea that you would like to talk about
You need to discuss stage equipment (rental) business'),
	('Travel Assistance', 'travel', 'travel@eurofurence.org','You have questions about the routes presented on your Travel &amp; Parking page
There are genius alternatives we don\'t know about yet'),
	('Fursuit Support', 'fursuit-support', 'fursuit-team@eurofurence.org','You own a fursuit and require assistance with it
You have questions or concerns about <i>suit</i>ability
You need further information about our fursuit lounge and suit related events'),
	('Photo Team', 'photo', 'photo@eurofurence.org','You want to become a member of the team
You have questions regarding the Fursuit Photography Gallery
Our services are needed for a special event'),
	('Media Relations', 'media-relations', 'media@eurofurence.org','You are from a media or news agent
You would like a tour or inquire filming rights'),
	('Public Affairs', 'public-affairs', 'pa@eurofurence.org','Organisations and Institutions interested in partnering with us
Businesses, Restaurants and other shops interested in cooperation during our convention
Civil and government services, to discussing local events and legal matters'),
	('Charity', 'charity', 'charity@eurofurence.org','Charity matters'),
	('Video & EF Prime', 'efprime', 'video@eurofurence.org','You would like to inquire special filming permissions
You would like to offer your skills to our film / sound / tech crew'),
	('Art Show', 'artshow', 'artshow@eurofurence.org','You are an artist (or agent) and would like to apply for a panel, or discuss your registration
You have noticed possible art theft
There are questions still open after reading our Art Show guide'),
	('Dealers\' Den', 'dealersden', 'dealers@eurofurence.org','You are an artist (or agent) and would like to sell your products in our den
There are questions concerning our Dealers\' Den or Artist Alley, which our Dealers\' Den page couldn\'t answer'),
	('Con Book', 'conbook', 'conbook@eurofurence.org','You would like to submit your artwork to the conbook
You have questions regarding your art submission or the conbook itself'),
	('Daily Eurofurence', 'daily', 'daily@eurofurence.org','There is a story or matter our daily newspaper would like to know about
You would like to announce something important on our paper
You are a layouter, photographer or writer and would like to join us'),
	('Advertising', 'ads', 'ads@eurofurence.org','Your convention would like to share con book ads with us
You need further guidence on format, design or measurements'),
	('Website', 'web', 'web@eurofurence.org','You find a bug or something looks weird on our website
You would like to offer a cool addition or idea
You\'ve realized the website is utterly bad and that needs to be said'),
	('IT Department', 'it', 'it@eurofurence.org','You need something technical during the convention
You have a highly technical question')
;

INSERT INTO `templates` (`department_id`, `reference`, `subject`, `message`) VALUES
	(7, 'submission', 'Event Submission', '* Title:
* Subtitle (optional):
* Panel/Event Host:
* Additional Host(s) (if any):
* Abstract (about 300 characters):

* Description:

* Remarks (if any, e.g. early slot, not on Tuesday...):

* Equipment:
* [ ] Flipchart
* [ ] Projector (Needed signal interface:)
 * [ ] VGA
 * [ ] DVI
 * [ ] DisplayPort
 * [ ] Mini-DisplayPort
 * [ ] HDMI
 * [ ] Mini-HDMI
* [ ] Power strip
* other:


')
;