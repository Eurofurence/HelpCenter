<?php
$this->layout = 'base-light';
$this->assign('title', 'Backend');

echo '<h1>Adminstration Login</h1>';

echo $this->Form->create();

echo $this->Form->input('username');
echo $this->Form->input('password');

echo '<p class="uk-align-right uk-text-small">';
echo '<a href="' . $resetUrl . '">Forgot Password</a> | ';
echo '<a href="' . $registerUrl . '">Create new Account</a>';
echo '</p>';



echo $this->Form->submit('Submit', [
	'type' => 'submit',
	'value' => 'Submit',
	'class' => 'uk-button uk-margin-top efhc-submit-button'
]);

echo $this->Form->end();
