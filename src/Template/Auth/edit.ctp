<?php
$this->layout = 'base-light';
$this->assign('title', 'Backend');


echo '<h1>Your Account</h1>';

echo $this->Form->create($user);
echo $this->Form->control('username');
echo $this->Form->control('email');

echo $this->Form->submit('Save', [
	'type' => 'submit',
	'value' => 'Submit',
	'class' => 'uk-button uk-margin-top efhc-submit-button'
]);

echo $this->Form->end();
?>

<div class="efhc-float-right">
	<a href="<?= $resetUrl ?>" class="uk-button uk-button-default">Reset Password</a>
	<a href="<?= $deleteUrl ?>" class="uk-button uk-button-danger" onclick="return confirm('Are you sure you want to delete your account? This is final!')">Delete Account</a>
</div>

