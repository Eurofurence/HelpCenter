<?php 
use Cake\Routing\Router;
$this->layout = 'base-light';
$this->assign('title', 'Backend');
?>

<h1><?= (count($departments) <= 1? 'Your Department' : 'Departments') ?></h1>

<?php

echo '<ul uk-accordion'.(count($departments) > 1? '>' : '="active: 0">');

foreach($departments as $d) {
	echo '<li>';
	echo '<h3 class="uk-accordion-title">' . ($d->name? h($d->name) : '<span class="efhc-icon-position-fix uk-text-muted" uk-icon="icon: plus-circle"></span> <span class="uk-text-muted">New Department</span>') . '</h3>';
	echo '<div class="uk-accordion-content">';
	echo $this->Form->create($d);
	echo $this->Form->control('id');
	echo $this->Form->control('name', ['required' => true]);
	echo $this->Form->control('reference', ['required' => true, 'label' => 'Public Contact Link (eurofurence.org/contact/*):']);
	echo $this->Form->control('email', ['required' => true, 'label' => 'eMail']);
	echo $this->Form->control('hint', ['required' => true, 'label' => 'Hint ("Contact this department, when...")']);

	if ($d->id !== null) {

		if ($admin) {
			$deleteToken = md5(uniqid(rand(), true));
			$this->request->session()->write('DeleteTokens.Departments.' . $d->id, $deleteToken);
			echo '<a href="' . $deleteUrl . '/' . $d->id . '/' . $deleteToken . '" class="uk-button uk-button-danger efhc-float-right efhc-admin-delete" onclick="return confirm(\'You are about to permanently delete this department. Proceed?\')"><span class="efhc-icon-position-fix" uk-icon="icon: trash"></span> DELETE</a>';
		}

		echo '<p>Editors of this Department:<ul>';
		foreach($d->users as $u) {
			echo '<li>' . $u->username . '</li>';
		}
		echo '</ul></p>';

		echo '<p>Created ' . $d->created . '</p>';
		echo '<p>Modified ' . $d->modified . '</p>';

		$url =Router::url(['controller' => 'contact', 'action' => 'index', h($d->reference)], true);

		echo '<p><strong>Public Contact Link: </strong><a href="' . $url . '" target="_blank">' . $url . '</a></p>';
			
	}

	echo $this->Form->submit('Save', [
		'type' => 'submit',
		'value' => 'Submit',
		'class' => 'uk-button uk-margin-top efhc-submit-button',
	]);
	echo $this->Form->end();
	echo '</div>';
	echo '</li>';
}

echo '</ul>'; // uk-accordion