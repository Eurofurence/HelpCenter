<?php
$this->layout = 'base-light';
$this->assign('title', 'Backend');
?>

<h1>
	Welcome, <?= h($userName) ?>!
	<a href="<?= $editDepartmentsUrl ?>" class="uk-icon-button efhc-float-right uk-margin-small-top uk-margin-left" uk-icon="icon: cog" uk-tooltip title="Edit your Departments"></a> 
	<a href="<?= $editUserUrl ?>" class="uk-icon-button efhc-float-right uk-margin-small-top" uk-icon="icon: user" uk-tooltip title="Edit your Account"></a>
</h1>
<p>Your department<?= (count($userDepartments) > 1? 's':'') ?>: 
<?php 
	$i = count($userDepartments); 
	foreach($userDepartments as $d) { 
		echo h($d->name) . ($i === 1? '':', '); 
		$i--; 
	}
?>
</p>


<h3>Your Department<?= (count($userDepartments) > 1? 's\'':'\'s') ?> Stats</h3>
<table class="uk-table">
	<tbody>
		<tr>
			<td><a href="<?= $templatesUrl ?>">eMail Templates</a></td>
			<td><?= $userTemplatesCount ?></td>
		</tr>
		<tr>
			<td><a href="<?= $articlesUrl ?>">FAQ Articles</a></td>
			<td><?= $userArticlesCount ?></td>
		</tr>
		<tr>
			<td><a href="<?= $disabledArticlesUrl ?>">Disabled FAQ Articles &amp; Suggestions</a></td>
			<td><?= $userDisabledArticlesCount ?></td>
		</tr>
	</tbody>
</table>

<?php if ($userRole === 'admin') { ?>
		
<h3>General Stats</h3>
<table class="uk-table">
	<tbody>
		<tr>
			<td><a href="<?= $departmentsUrl ?>">Departments</a></td>
			<td><?= $globalDepartmentsCount ?></td>
		</tr>
		<tr>
			<td><a href="<?= $usersUrl ?>">Backend Users</a></td>
			<td><?= $globalUsersCount ?></td>
		</tr>
		<tr <?= ($globalDisabledUsersCount > 0? ' class="efhc-highlight"' : '') ?>>
			<td><a href="<?= $usersUrl ?>">Users Awaiting Activation</a></td>
			<td><?= $globalDisabledUsersCount ?></td>
		</tr>
		<tr>
			<td><a href="<?= $templatesUrl ?>">eMail Templates</a></td>
			<td><?= $globalTemplatesCount ?></td>
		</tr>
		<tr>
			<td><a href="<?= $articlesUrl ?>">FAQ Articles</a></td>
			<td><?= $globalArticlesCount ?></td>
		</tr>
		<tr>
			<td><a href="<?= $disabledArticlesUrl ?>">Disabled FAQ Articles &amp; Suggestions</a></td>
			<td><?= $globalDisabledArticlesCount ?></td>
		</tr>
		<tr>
			<td><a href="<?= $textsUrl ?>">Website Texts</a></td>
			<td><?= $globalTextsCount ?></td>
		</tr>
	</tbody>
</table>

<?php } //admin role ?>
