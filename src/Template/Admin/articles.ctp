<?php
$this->layout = 'base-light';
$this->assign('title', 'Backend');
?>

<h1>FAQ Articles</h1>

<a href="<?= $switchViewUrl ?>" class="uk-button uk-button-primary"><?= $switchViewText ?></a>

<?php


echo '<ul uk-accordion' . (count($articles) > 1? '>' : '="active: 0">');

foreach($articles as $a) {
	echo '<li>';
	echo '<h3 class="uk-accordion-title'.((!$a->enabled)? ' efhc-red' : '').'">' . ($showDepartmentPrefix && $a->department->name ? '[' . h($a->department->name) . '] ' : '') . ($a->question? h($a->question) : '<span class="efhc-icon-position-fix uk-text-muted" uk-icon="icon: plus-circle"></span> <span class="uk-text-muted">New Article</span>') . '</h3>';
	echo '<div class="uk-accordion-content">';
	echo $this->Form->create($a);
	echo $this->Form->control('id');
	echo $this->Form->control('notes', ['label' => 'Internal Notes']);
	echo $this->Form->control('question', ['required' => true]);
	echo $this->Form->control('answer', ['required' => true]);
	echo $this->Form->control('enabled', ['default' => true, 'label' => 'Visible', 'id' => $a->id]);

	if ($a->department->id === null) {
		$a->department->id = $firstUserDepartmentId;
	}
	echo $this->Form->input('Departments', [
		'type' => 'select', 
		'options' => $departments,
		'default' => $a->department->id,
		'label' => 'Assigned Department',
		'onchange' => 'UIkit.notification("Warning: be aware that assigning another department than your own will move this article out of your reach.", {status: "warning", timeout : 15000})'
	]);
	
	if ($a->id !== null) {
		$deleteToken = md5(uniqid(rand(), true));
		$this->request->session()->write('DeleteTokens.Articles.' . $a->id, $deleteToken);
		echo '<a href="' . $deleteUrl . '/' . $a->id . '/' . $deleteToken . '" class="uk-button uk-button-danger efhc-float-right efhc-admin-delete" onclick="return confirm(\'You are about to permanently delete this article. Proceed?\')"><span class="efhc-icon-position-fix" uk-icon="icon: trash"></span> DELETE</a>';

		echo '<p>Created ' . $a->created . '</p>';
		echo '<p>Modified ' . $a->modified . '</p>';
	}

	echo $this->Form->submit('Save', [
		'type' => 'submit',
		'value' => 'Submit',
		'class' => 'uk-button uk-margin-top efhc-submit-button',
	]);
	echo $this->Form->end();
	echo '</div>';
	echo '</li>';
}

echo '</ul>'; // uk-accordion