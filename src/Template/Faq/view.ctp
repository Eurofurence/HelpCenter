<?php

use Cake\I18n\Time;

$this->layout = 'base-light';
$this->assign('title', 'FAQ');

$question = $data['question'];
$answer = $data['answer'];
$created = new Time($data['created']);
$modified = new Time($data['modified']);
$department = $data['department']['name'];


$created = $created->format('d.m.Y');
$modified = $modified->format('d.m.Y');

echo '<div class="uk-card uk-card-default">';

echo '<div class="uk-card-body">';
echo '<h3 class="uk-card-title">' . $question . '</h3>';
echo '<p class="efhc-preserve-newlines">' . $answer . '</p>';
echo '</div>'; // .uk-card-body

echo '<div class="uk-card-footer">';
if ($created == $modified) {
	echo '<p class="uk-text-small">' . $created . ', ';
}
else {
	echo '<p class="uk-text-small">' . $created . ' &ndash; ' . $modified . ', ';
}

echo '<a href="' . $departmentUrl . '" class="efhc-contact">' . $department . '</a>';
echo '</p>';
echo '</div>'; // .uk-card-footer

echo '</div>'; // .uk-card

// debug($data);
