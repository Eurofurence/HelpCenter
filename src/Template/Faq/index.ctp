<?php
$this->layout = 'base-light';
$this->assign('title', 'FAQ');
?>

<noscript>
	<div id="efhc-js-warning">
		<p>Sorry, this page requires JavaScript to function.</p>
	</div>
</noscript>

<?php

echo $this->Form->create(null, [
	'id' => 'efhc-faqSearchForm',
	'class' => ['uk-search','uk-width-1-1'],
	'url' => $target,
	'onsubmit' => 'javascript: return false'
]);

echo $this->Form->input('Search', [
	'placeholder' => 'What is your question about?',
	'label' => '',
	'class' => 'uk-search-input',
	'autofocus'
]);

echo '<ul uk-accordion>';
echo '<li>';
echo '<h4 class="uk-accordion-title">Search Options</h4>';
echo '<div class="uk-accordion-content">';

echo '<div class="efhc-checkbox">';
echo $this->Form->checkbox('Question', [
	'checked' => true,
	'id' => 'Question'
]);
echo '<label for="Question">in Questions</label>';
echo '</div>';

echo '<div class="efhc-checkbox">';
echo $this->Form->checkbox('Answer', [
	'checked' => true,
	'id' => 'Answer'
]);
echo '<label for="Answer">in Answers</label>';
echo '</div>';

echo '<p>Topics<p>';
echo $this->Form->input('Departments', [
	'type' => 'select', 
	'multiple' => 'checkbox', 
	'options' => $departments,
	'label' => ''
]);


echo $this->Form->button('Select All', [
	'type' => 'button',
	'class' => ['uk-button', 'uk-button-default', 'uk-button-small', 'uk-margin-top' ],
	'onClick' => 'selectAllDepartments()'
]);
echo $this->Form->button('Unselect All', [
	'type' => 'button',
	'class' => ['uk-button', 'uk-button-default', 'uk-button-small', 'uk-margin-top' ],
	'onClick' => 'unselectAllDepartments()'
]);


echo $this->Form->Button('Refresh', [
	'type' => 'button',
	'class' => ['uk-button', 'uk-button-primary', 'uk-button-small', 'uk-margin-top', 'uk-margin-left'],
	'onClick' => 'faqSearch()'
]);

echo '</div>'; // .uk-accordion-content
echo '</li>';
echo '</ul>'; // uk-accordion

echo $this->Form->end();

echo '<hr />';

echo '<ul class="uk-pagination uk-flex-between efhc-pagination">';
echo '<li class="efhc-pagination-previous"><a href="#"><span class="uk-margin-small-right" uk-pagination-previous></span> Previous</a></li>';
echo '<li class="uk-disabled"><span class="efhc-queryStatus"></span></li>';
echo '<li class="efhc-pagination-next"><a href="#">Next <span class="uk-margin-small-left" uk-pagination-next></span></a></li>';
echo '</ul>'; // .uk-pagination .uk-flex-between .efhc-pagination

echo '<ul uk-accordion id="efhc-faqResults">';
echo '</ul>'; // #efhc-faqResults

?>

<script type="text/javascript">
	$('#efhc-js-warning').hide();
</script>

<script type="text/javascript">
	var searchTerm = '';

	window.onload=function(){
		selectAllDepartments();
		faqSearch();
	}

	function selectAllDepartments() {
		$('input[id^="departments-"]').prop('checked', true);
	}

	function unselectAllDepartments() {
		$('input[id^="departments-"]').prop('checked', false);
	}


	$("#search").on("change keyup paste", prepareFaqSearch);

	function prepareFaqSearch() {
		query = $("#search").val();
		if (query != searchTerm) {
			searchTerm = query;
			faqSearch();
		}
	}

	function debounce(fct, wait) {
		var timeout;
		return function() {
			var ctx = this, args = arguments;
			clearTimeout(timeout);
			timeout = setTimeout(function() {
				timeout = null;
				fct.apply(ctx, args);
			}, wait);
		};
	}

	var faqCall = debounce(function(page, url, post_data) {
		$.ajax({
			type: 'post',
			url: url + '?page=' + page,
			data: post_data,
			success: function(data, textStatus, jqXHR) {
				
				// UIkit.modal.alert(jqXHR.responseText);

				var json = JSON.parse(jqXHR.responseText);

				displayResults(json['data']);

				displayPageControl(json['pagination']);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				// UIkit.notification({
				// 	// message : '<i class=\'uk-icon-times-circle\'></i> ' + jqXHR.responseText,
				// 	message : 'Oops, something bad happened. Would you be so kind and file a bug report to web@eurofurence.org? Please include what you did!',
				// 	status  : 'danger',
				// 	timeout : 0,
				// 	pos     : 'top-center'
				// });
			},
			dataType: 'text',
			complete: function(jqXHR, textStatus) {
				// alert(jqXHR.responseText);
			}
		});
	}, 300);

	function faqSearch(page/* = 1*/) {
		var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1; // shitty IE compatibility workaround
		queryStatus('loading...');

		var url = '<?php echo $target ?>';
		var post_data = $('#efhc-faqSearchForm').serializeArray();

		// UIkit.notification(query, {status: 'success'});
		faqCall(page, url, post_data);
	}

	function displayResults(ResultsJson) {
		$('#efhc-faqResults').empty();
		jQuery.each(ResultsJson, appendResult);
	}

	function appendResult(i, val) {
		var q = val['question'];
		var a = val['answer'];
		var permalink = '<?php echo $permalink; ?>/' + val['id']; 

		// <li>
		// 	  <h3 class="uk-accordion-title">$question</h3>
		// 	  <div class="uk-accordion-content">$answer</div>
		// </li>

		var li  = document.createElement('li');
		var h3  = document.createElement('h3');
		var div = document.createElement('div');
		var ref = document.createElement('a');

		h3.className  = 'uk-accordion-title';
		div.className = 'uk-accordion-content efhc-preserve-newlines';
		ref.className = 'uk-icon-button efhc-permalink';
		ref.setAttribute('href', permalink);
		ref.setAttribute('uk-icon', 'icon: link');

		// h3.appendChild(document.createTextNode(q));
		// div.appendChild(document.createTextNode(a));
		$(h3).html(q);
		$(div).html(a);
		div.appendChild(ref);

		li.appendChild(h3);
		li.appendChild(div);

		$('#efhc-faqResults').append(li);
	}

</script>

<script type="text/javascript">
	function displayPageControl(paginationJson) {
		var page = paginationJson['page'];
		var pageCount = paginationJson['pageCount'];
		var count = paginationJson['count'];

		switch(count) {
			case 0 : queryStatus('No results'); break;
			case 1 : queryStatus('One result'); break;
			default: queryStatus('Page ' + page + ' of ' + pageCount + ' (for ' + count + ' results)'); 
		}

		paginationPrevious(paginationJson);
		paginationNext(paginationJson);
	}

	function paginationPrevious(paginationJson) {
		if (paginationJson['hasPrev']) {
			var target = 'faqSearch(' + (paginationJson['page'] - 1) + ')';
			$('.efhc-pagination-previous').removeClass('uk-disabled').addClass('uk-enabled');
			$('.efhc-pagination-previous > a').attr('onClick', 'faqSearch(' + (paginationJson['page'] - 1) + ')');
		}
		else {
			$('.efhc-pagination-previous').removeClass('uk-enabled').addClass('uk-disabled');
			$('.efhc-pagination-previous > a').attr('onClick', '');
		}
	}
	
	function paginationNext(paginationJson) {
		if (paginationJson['hasNext']) {
			var target = 'faqSearch(' + (paginationJson['page'] + 1) + ')';
			$('.efhc-pagination-next').removeClass('uk-disabled').addClass('uk-enabled');
			$('.efhc-pagination-next > a').attr('onClick', target);
		}
		else {
			$('.efhc-pagination-next').removeClass('uk-enabled').addClass('uk-disabled');
			$('.efhc-pagination-next').attr('onClick', '');
		}
	}

	function queryStatus(str) {
		$('.efhc-queryStatus').text(str);
	}
	
</script>

<?php if (isset($autosearch)) : ?>
<script>
	document.getElementById('search').value = '<?= $autosearch ?>';
	prepareFaqSearch();
</script>
<?php endif; ?> 
