<?php

use Cake\I18n\Time;

$this->layout = 'base-light';
$this->assign('title', 'FAQ');


echo $this->Form->create(null, [
	'class' => ['uk-width-1-1']
]);

echo '<div uk-grid="margin:uk-margin-small">';

echo '<div class="uk-width-1-1@s">';
echo $this->Form->input('department_id', [
	'type' => 'select',
	'options' => $department_id,
	'default' => 0,
	'label' => 'Please choose a topic for your question',
	'id' => 'efhc-switchControl',
	'class' => 'uk-select uk-margin-bottom',
	'autofocus'
]);
echo '</div>'; // .uk-width-1-1@s

// echo '<div class="uk-width-1-2@s">';
// echo $this->Form->input('Name', [
// 	'label' => 'Your Name',
// 	'class' => 'uk-input uk-margin-bottom',
// 	'placeholder' => 'Your Name (optional)'
// ]);
// echo '</div>'; // .uk-width-1-2@s

// echo '<div class="uk-width-1-2@s">';
// echo $this->Form->input('Mail', [
// 	'label' => 'Your eMail address',
// 	'class' => 'uk-input uk-margin-bottom',
// 	'placeholder' => 'Your eMail address (optional)'
// ]);
// echo '</div>'; // .uk-width-1-2@s

echo '<div class="uk-width-1-1@s">';
echo $this->Form->input('question', [
	'label' => 'Your Question',
	'class' => 'uk-input',
	'placeholder' => 'What question would you like to suggest?'
]);
echo '</div>'; // .uk-width-1-1@s

echo '</div>'; // uk-grid

echo '
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="g-recaptcha" data-sitekey="' . $gSitekey . '"></div>
<noscript>
  <div>
	<div style="width: 302px; height: 422px; position: relative;">
	  <div style="width: 302px; height: 422px; position: absolute;">
		<iframe src="https://www.google.com/recaptcha/api/fallback?k=' . $gSitekey . '"
				frameborder="0" scrolling="no"
				style="width: 302px; height:422px; border-style: none;">
		</iframe>
	  </div>
	</div>
	<div style="width: 300px; height: 60px; border-style: none;
				   bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px;
				   background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
	  <textarea id="g-recaptcha-response" name="g-recaptcha-response"
				   class="g-recaptcha-response"
				   style="width: 250px; height: 40px; border: 1px solid #c1c1c1;
				   margin: 10px 25px; padding: 0px; resize: none;" >
	  </textarea>
	</div>
  </div>
</noscript>
';

echo $this->Form->submit('Submit', [
	'type' => 'submit',
	'value' => 'Submit',
	'class' => 'uk-button uk-margin-top efhc-submit-button'
]);

$this->Form->unlockField('g-recaptcha-response'); // remove this field from CakePHP's security check
echo $this->Form->end();

?>

<div class="uk-card uk-card-default uk-margin-top">
	<div class="uk-card-body">
		<h3 class="uk-card-title">Some tips on creating good questions:</h3>
		<ul>
			<li>Is your question universally valid? Please avoid too in-depth questions on specific, personal issues.</li>
			<li>You may add invisible keywords to your question - simply prepend a # to each of them. <br />Example: <i>What's salmon? #fish #sushi</i></li>
			<li>Take your time to refine your wording. This isn't a race, and a well-worded question has a higher chance to be approved.</li>
		</ul>
	</div>
</div>	

<script type="text/javascript">
	$('#efhc-js-warning').hide();
</script>