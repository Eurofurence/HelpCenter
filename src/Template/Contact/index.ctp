<?php
$this->layout = 'base-light';
$this->assign('title', 'Contact');

// prepare department hints
$departmentHintsHtml = '<ul id="efhc-departmentHints" class="uk-switcher">';
foreach ($departmentHints as $departmentId => $dhs) {
	$dhs = explode(PHP_EOL, $dhs);
	$departmentHintsHtml .= '<li><ul>';
	foreach ($dhs as $dh) {
		if (empty($dh))
			continue;
		$departmentHintsHtml .= '<li>' . h($dh) . '</li>'; 
	}
	$departmentHintsHtml .= '</ul>';
	if(array_key_exists($departmentId, $templates)) {
		$departmentHintsHtml .= '<br /> Templates <ul>';
		foreach($templates[$departmentId] as $templ) {
			$departmentHintsHtml .= '<li><a onclick="fillForm(`' . str_replace("`", "\`", $templ->subject) . '`, `' .  str_replace("`", "\`", $templ->message) .'`)">';
			$departmentHintsHtml .= $templ->subject;
			$departmentHintsHtml .= '</a></li>';
		}
		$departmentHintsHtml .= '</ul>';
	}

	$departmentHintsHtml .= '</li>';
}
$departmentHintsHtml .= '</ul>';

?>

<noscript>
	<div id="efhc-js-warning">
		<p>Please note: this page works best with JavaScript enabled.</p>
	</div>
</noscript>

<?php

echo $this->Form->create(null, [
	'class' => ['uk-width-1-1']
]);

echo '<div uk-grid="margin:uk-margin-small">';
echo '<div class="uk-width-1-2@s">';
echo $this->Form->input('Departments', [
	'type' => 'select',
	'options' => $departments,
	'default' => $select,
	'label' => 'Choose a Department',
	'id' => 'efhc-switchControl',
	'class' => 'uk-select uk-margin-bottom',
	'uk-switcher' => 'connect: #efhc-departmentHints; animation: uk-animation-fade',
	'autofocus'
]);

echo $this->Form->input('Name', [
	'label' => 'Your Name',
	'class' => 'uk-input uk-margin-bottom',
	'placeholder' => 'How should we call you?'
]);

echo $this->Form->input('Mail', [
	'label' => 'Your eMail address',
	'class' => 'uk-input uk-margin-bottom',
	'placeholder' => 'How do we reach you back?'
]);

echo $this->Form->input('Subject', [
	'label' => 'Subject',
	'class' => 'uk-input',
	'placeholder' => 'What is your message about?'
]);
echo '</div>'; // .uk-width-1-2@s

echo '<div class="uk-width-1-2@s">';
echo '<span class="efhc-departmentHintsIntro">Contact this department, when…</span>';
echo $departmentHintsHtml;
echo '</div>'; // .uk-width-1-2@s

echo '<div class="uk-width-1-1@s">';
echo $this->Form->input('Message', [
	'type' => 'textarea',
	'label' => 'Your message',
	'class' => 'uk-textarea',
	'rows' => '10',
	'placeholder' => 'What can we do for you?'
]);
echo '</div>'; // .uk-width-1-1@s

echo '</div>'; // uk-grid

echo '
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="g-recaptcha" data-sitekey="' . $gSitekey . '"></div>
<noscript>
  <div>
	<div style="width: 302px; height: 422px; position: relative;">
	  <div style="width: 302px; height: 422px; position: absolute;">
		<iframe src="https://www.google.com/recaptcha/api/fallback?k=' . $gSitekey . '"
				frameborder="0" scrolling="no"
				style="width: 302px; height:422px; border-style: none;">
		</iframe>
	  </div>
	</div>
	<div style="width: 300px; height: 60px; border-style: none;
				   bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px;
				   background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
	  <textarea id="g-recaptcha-response" name="g-recaptcha-response"
				   class="g-recaptcha-response"
				   style="width: 250px; height: 40px; border: 1px solid #c1c1c1;
				   margin: 10px 25px; padding: 0px; resize: none;" >
	  </textarea>
	</div>
  </div>
</noscript>
';

echo $this->Form->submit('Submit', [
	'type' => 'submit',
	'value' => 'Submit',
	'class' => 'uk-button uk-margin-top efhc-submit-button'
]);

$this->Form->unlockField('g-recaptcha-response'); // remove this field from CakePHP's security check
echo $this->Form->end();

?>

<script type="text/javascript">
	$('#efhc-js-warning').hide();
</script>

<script type="text/javascript">

	UIkit.switcher('#efhc-switchControl').show(<?= $select ?>);

	document.querySelector('#efhc-switchControl').addEventListener('change', function(evt) {
		UIkit.switcher('#efhc-switchControl').show(evt.target.value);
	});
</script>

<script type="text/javascript">

	function fillForm(subject, message) {
		document.getElementById('subject').value = subject;
		document.getElementById('message').value = message;
	}
</script>