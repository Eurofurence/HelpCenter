<?php
use Cake\Routing\Router;

/**
 * Eurofurence Help Center
 * Base Light Layout
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    draconigen@gmail.com
 * @link      https://www.eurofurence.org/help/
 * @since     0.1
 * @license   https://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Eurofurence Help Center:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('uikit.min.css') ?>
    <?= $this->Html->css('efhelp_basewhite.css') ?>
    
    <?= $this->Html->script('jquery-3.2.1.min.js') ?>
    <?= $this->Html->script('uikit.min.js') ?>
    <?= $this->Html->script('uikit-icons.min.js') ?>
    <?= $this->Html->script('efhc-functions.js') ?>
    

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <header>
        <div class="efhc-container">
            <h1>Eurofurence Help Center &ndash; <?= $this->fetch('title') ?></h1>
        </div>
    </header>
    <?php
        $act['contact'] = $act['faq'] = $act['legal'] = $act['admin'] = '';

        switch([strtolower($this->request->controller)]) {
            case ['contact']: $act['contact'] = 'efhc-active'; break;
            case ['faq']    : $act['faq'] = 'efhc-active'; break;
            case ['legal']  : $act['legal'] = 'efhc-active'; break;
            case ['admin']  : $act['admin'] = 'efhc-active'; break;
        }
    ?>
    <nav role="navigation">
        <ul>
            <li><a href="<?= Router::url(['controller' => 'contact', 'action' => 'index'], true) ?>" class="<?= $act['contact'] ?>">Contact</a></li>
            
            <li><a href="#" class="<?= $act['faq'] ?>">FAQ<span uk-icon="icon:triangle-down"></span></a>
                <ul>
                    <li><a href="<?= Router::url(['controller' => 'faq', 'action' => 'index'], true) ?>" class="<?= $act['faq'] ?>">Questions &amp; Answers</a></li>
                    <li><a href="<?= Router::url(['controller' => 'faq', 'action' => 'suggest'], true) ?>" class="<?= $act['faq'] ?>">Suggest a Question</a></li>
                </ul>
            </li>

            <li><a href="#" class="<?= $act['legal'] ?>">Legal<span uk-icon="icon:triangle-down"></span></a>
                <ul>
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'imprint'], true) ?>" class="<?= $act['legal'] ?>">Imprint</a></li>
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'privacy'], true) ?>" class="<?= $act['legal'] ?>">Privacy Statement</a></li>
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'roc'], true) ?>" class="<?= $act['legal'] ?>">Rules of Conduct</a></li>
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'terms'], true) ?>" class="<?= $act['legal'] ?>">Terms and Conditions</a></li>
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'liability'], true) ?>" class="<?= $act['legal'] ?>">Liability Waiver</a></li>
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'statutes'], true) ?>" class="<?= $act['legal'] ?>">Statutes of Eurofurence e.V.</a></li>
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'attributions'], true) ?>" class="<?= $act['legal'] ?>">Site Attributions</a></li>
                </ul>
            </li>

            <?php if ($this->request->session()->read('Auth.User.id')) { ?>
                <li><a href="<?= Router::url(['controller' => 'admin', 'action' => 'index'], true) ?>" class="<?= $act['admin'] ?>">Administration<span uk-icon="icon:triangle-down"></span></a>
                    <ul>
                        <li><a href="<?= Router::url(['controller' => 'admin', 'action' => 'index'], true) ?>" class="<?= $act['admin'] ?>">Overview</a></li>
                        <li><a href="<?= Router::url(['controller' => 'auth', 'action' => 'edit'], true) ?>" class="<?= $act['admin'] ?>">Your Account</a></li>
                        <li><a href="<?= Router::url(['controller' => 'admin', 'action' => 'departments'], true) ?>" class="<?= $act['admin'] ?>">Departments</a></li>
                        <li><a href="<?= Router::url(['controller' => 'admin', 'action' => 'templates'], true) ?>" class="<?= $act['admin'] ?>">eMail Templates</a></li>
                        <li><a href="<?= Router::url(['controller' => 'admin', 'action' => 'articles'], true) ?>" class="<?= $act['admin'] ?>">FAQ Articles</a></li>
                        <li><a href="<?= Router::url(['controller' => 'admin', 'action' => 'articles', 'disabled'], true) ?>" class="<?= $act['admin'] ?>">Disabled FAQ &amp; Suggestions</a></li>
                        <?php if ($this->request->session()->read('Auth.User.role') === 'admin') { ?>
                            <li><a href="<?= Router::url(['controller' => 'admin', 'action' => 'texts'], true) ?>" class="<?= $act['admin'] ?>">Website Texts</a></li>
                            <li><a href="<?= Router::url(['controller' => 'auth', 'action' => 'users'], true) ?>" class="<?= $act['admin'] ?>">Edit Users</a></li>
                        <?php } ?>
                        <li><a href="<?= Router::url(['controller' => 'auth', 'action' => 'logout'], true) ?>">Logout</a></li>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </nav>

    <?= $this->Flash->render() ?>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer class="efhc-light">
        <div class="efhc-container">
            <div>
                <h3>Contact us:</h3>
                <ul class="uk-list">
                    <li class="efhc-light"><a href="<?= Router::url(['controller' => 'contact','action' => 'index'], true) ?>" class="efhc-contact">Contact Center</a></li>
                    <li class="efhc-light"><a href="<?= Router::url(['controller' => 'contact','action' => 'index', 'web'], true) ?>" class="efhc-contact">Website Support</a></li>
                </ul>
            </div>
            <div>
                <h3>Find us on:</h3>
                <div class="uk-button-group">
                    <a target="_blank" href="https://www.eurofurence.org/" class="uk-icon-button" uk-icon="icon:home"></a> 
                    <a target="_blank" href="https://www.twitter.com/eurofurence" class="uk-icon-button" uk-icon="icon:twitter"></a> 
                    <a target="_blank" href="https://www.facebook.com/eurofurence" class="uk-icon-button" uk-icon="icon:facebook"></a> 
                    <a target="_blank" href="https://discord.com/invite/VMESBMM" class="uk-icon-button ef-icon-injection">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 245 240"><path d="M104.4 103.9c-5.7 0-10.2 5-10.2 11.1s4.6 11.1 10.2 11.1c5.7 0 10.2-5 10.2-11.1.1-6.1-4.5-11.1-10.2-11.1zM140.9 103.9c-5.7 0-10.2 5-10.2 11.1s4.6 11.1 10.2 11.1c5.7 0 10.2-5 10.2-11.1s-4.5-11.1-10.2-11.1z"/><path d="M189.5 20h-134C44.2 20 35 29.2 35 40.6v135.2c0 11.4 9.2 20.6 20.5 20.6h113.4l-5.3-18.5 12.8 11.9 12.1 11.2 21.5 19V40.6c0-11.4-9.2-20.6-20.5-20.6zm-38.6 130.6s-3.6-4.3-6.6-8.1c13.1-3.7 18.1-11.9 18.1-11.9-4.1 2.7-8 4.6-11.5 5.9-5 2.1-9.8 3.5-14.5 4.3-9.6 1.8-18.4 1.3-25.9-.1-5.7-1.1-10.6-2.7-14.7-4.3-2.3-.9-4.8-2-7.3-3.4-.3-.2-.6-.3-.9-.5-.2-.1-.3-.2-.4-.3-1.8-1-2.8-1.7-2.8-1.7s4.8 8 17.5 11.8c-3 3.8-6.7 8.3-6.7 8.3-22.1-.7-30.5-15.2-30.5-15.2 0-32.2 14.4-58.3 14.4-58.3 14.4-10.8 28.1-10.5 28.1-10.5l1 1.2c-18 5.2-26.3 13.1-26.3 13.1s2.2-1.2 5.9-2.9c10.7-4.7 19.2-6 22.7-6.3.6-.1 1.1-.2 1.7-.2 6.1-.8 13-1 20.2-.2 9.5 1.1 19.7 3.9 30.1 9.6 0 0-7.9-7.5-24.9-12.7l1.4-1.6s13.7-.3 28.1 10.5c0 0 14.4 26.1 14.4 58.3 0 0-8.5 14.5-30.6 15.2z"/></svg>
					</a>
                    <a target="_blank" href="https://vimeo.com/eurofurence" class="uk-icon-button" uk-icon="icon:vimeo"></a>
                </div>
            </div>
            <div>
                <h3>Legal:</h3>
                <ul class="uk-list">
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'imprint'], true) ?>"><span uk-icon="icon:bookmark"></span>Imprint &amp; Legal Notice</a></li>
                    <li><a href="<?= Router::url(['controller' => 'legal', 'action' => 'attributions'], true) ?>"><span uk-icon="icon:heart" class="efhc-big-icon-fix"></span>Site Attributions</a></li>
                    <li><a href="<?= Router::url(['controller' => 'admin', 'action' => 'index'], true) ?>"><span uk-icon="icon:sign-in" class="efhc-big-icon-fix"></span>Staff Login</a></li>
                    <!-- <li>&nbsp;</li> -->
                </ul>
            </div>
        </div> <!-- .container -->
    </footer>

    <script>beatifyNotifications(); </script>
    <?php include('ef-cookienotice.html'); ?>
</body>
</html>
