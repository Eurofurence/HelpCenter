<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class TextsTable extends Table
{
    public function initialize(array $config)
    {
        $this->belongsTo('Users');
        $this->addBehavior('Timestamp');
    }
}