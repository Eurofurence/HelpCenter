<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class ArticlesTable extends Table
{
    public function initialize(array $config)
    {
        $this->belongsTo('Departments');
        $this->addBehavior('Timestamp');
        // $this->addBehavior('Chris48s/Searchable.Searchable');
    }
}