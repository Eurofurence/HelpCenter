<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class SuggestionsTable extends Table
{
    public function initialize(array $config)
    {
        $this->belongsTo('Departments');
        $this->addBehavior('Timestamp');
    }
}