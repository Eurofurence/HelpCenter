<?php
/**
 * Eurofurence Help Center
 * Authentication Controller
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    draconigen@gmail.com
 * @link      https://www.eurofurence.org/help/auth
 * @since     1.0
 * @license   https://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Component\AuthComponent;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use Cake\I18n\FrozenTime;

class AuthController extends AppController
{
    public function initialize() {
        parent::initialize();
        $this->Auth->allow(['logout', 'register', 'reset', 'password']);
    }

    public function index() {
        return $this->redirect(['action' => 'login']);
    }

    public function login() {
        $usersCtx = TableRegistry::get('Users');

        if ($this->Auth->user('id'))
            return $this->redirect(['controller' => 'admin', 'action' => 'index']);

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user && $user['enabled']) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error('Authentication failed.');
            }
        }

        $this->set('registerUrl', Router::url(['action' => 'register'], true));
        $this->set('resetUrl', Router::url(['action' => 'reset'], true));
    }

    public function logout() {
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }

    public function register() {
        $usersCtx = TableRegistry::get('Users');
        $this->set('usersCtx', $usersCtx);

        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if (!$usersCtx->find('all')->where(['OR' => ['email' => $data['email'], 'username' => $data['username']]])->isEmpty()) { 
                $this->Flash->error('It seems there is an account with this Name or eMail address already present. Please contact website support if you have troubles logging in.');
                return;
            }

            if ($data['passwd1'] !== $data['passwd2']) {
                $this->Flash->error('Both passwords supplied don\'t match! Please check again.');
                return;
            }


            $user = $usersCtx->newEntity($data);

            $user->password = $data['passwd1'];
            
            if ($usersCtx->save($user)) {
                Email::deliver(null, '[EFHC] A new admin account needs to be reviewed', $user->username . ' has registered for the EFHC administration backend. Review and activate accounts at: ' . Router::url(['controller' => 'auth', 'action' => 'users'], true), 'admin');
                $this->Flash->modal('Your registration has been saved and will be reviewed shortly. You can log in as soon as your account has been enabled.');
                return $this->redirect(['controller' => 'admin']);
            } else {
                $this->Flash->error('Credentials refused. Please check your input or contact the system administrator.');
            }
        }
    }

    public function edit() {
        $usersCtx = TableRegistry::get('Users');
        $user = $usersCtx->get($this->Auth->user('id'));

        if ($this->request->is(['post', 'put'])) {
            $this->request->data['id'] = $user->id;
            $user = $usersCtx->newEntity($this->request->getData());

            if ($usersCtx->save($user)) {
                $this->Flash->success('Account updated.');
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error('Update failed! Please try again or contact the website administrator.');
            }
        }

        $this->set('user', $user);
        $this->set('resetUrl', Router::url(['action' => 'reset'], true));
        $this->set('deleteUrl', Router::url(['action' => 'delete'], true));
        $this->set('admin', $this->Auth->user('role') === 'admin');
    }

    public function reset() {
        $usersCtx = TableRegistry::get('Users');

        if ($this->request->is('post') || !empty($this->Auth->user())) {

            if ($this->request->is('post')) {
                $email = $this->request->getData('email');
                $user = $usersCtx->find('all')->where(['email' => $email])->order(['id' => 'desc'])->limit(1)->first();
            }
            else {
                $user = $usersCtx->get($this->Auth->user('id'));
                $email = $user->email;
            }
           
            if (!empty($user)) {
                $user = $usersCtx->get($user->id);
                $username = $user->username;
                $token = md5(uniqid(rand(), true));
                $user->token = $token; 
                $usersCtx->save($user);

                $subject = '[EFHC] Your password reset request';
                $newPwUrl = Router::url(['controller' => 'auth', 'action' => 'password', $username, $token], true);
                $message = "Hello $username,\n\nYou have requested to reset your password. Please click on the following link (or copy & paste the URL into your browser's address bar and hit enter). You'll be asked to set yourself a new password:\n\n$newPwUrl\n\nIf you receive this message without asking for it, please inform the system administrator immediately!\n\nBest regards,\nThe Eurofurence Help Center";

                Email::deliver($email, $subject, $message, 'admin');
            }

            $this->Flash->modal('If the address provided exists, you will receive a password reset eMail shortly. Please follow the instructions within.');
            return $this->redirect(['controller' => 'admin']);
        }
    }

    public function password($username, $token) {
        $usersCtx = TableRegistry::get('Users');
        $user = $usersCtx->find('all')->where(['username' => $username, 'token' => $token])->order(['id' => 'desc'])->limit(1)->first();

        if (!empty($user)) {
            $this->set('user', $user);
            if ($this->request->is(['post', 'put'])) {
                $data = $this->request->getData();
                if ($data['passwd1'] !== $data['passwd2']) {
                    $this->Flash->error('Both passwords supplied don\'t match! Please check again.');
                    return;
                }

                if ($user->modified->wasWithinLast('24 hours')) {
                    $user->password = $data['passwd1'];
                    $this->Flash->success('Your password has been changed.');
                }
                else {
                    $this->Flash->error('Your password reset token has expired. Please initiate a new Password reset process.');
                }

                $user->token = null;
                $usersCtx->save($user);
                return $this->redirect(['controller' => 'admin', 'action' => 'index']);
            }
        }
        else {
            $this->Flash->error('Sorry, something went wrong. Please try again or contact the website administrator.');
            $this->redirect(['controller' => 'admin', 'action' => 'index']);
        }        
    }

    public function delete($id = null) {
        $usersCtx = TableRegistry::get('Users');
        if ($this->Auth->user('role') === 'admin' && $id !== null) {
            $user = $usersCtx->get($id);
        }
        else {
            $user = $usersCtx->get($this->Auth->user('id'));
        }

        if ($usersCtx->delete($user)) {
            $this->Flash->success('Account deleted.');
            if ($this->Auth->user('role') === 'admin')
                return $this->redirect(['controller' => 'auth', 'action' => 'users']);
            else 
                return $this->redirect($this->Auth->logout());
        }
        else {
            $this->Flash->error('Sorry, something went wrong. Please contact the website administrator about it.');
        }
    }

    public function users() {
        if ($this->Auth->user('role') !== 'admin') {
            $this->Flash->error('Access denied.');
            return $this->redirect(['controller' => 'admin', 'action' => 'index']);
        }

        $usersCtx = TableRegistry::get('Users');
        $departmentsCtx = TableRegistry::get('Departments');

        if ($this->request->is(['post', 'put'])) {
            $user = $usersCtx->newEntity($this->request->getData());

            $user->departments = $departmentsCtx->find('all')->where(['id' => $user->departmentsList], ['id' => 'integer[]'])->toArray();

            if ($usersCtx->save($user)) {
                $this->Flash->success('User updated.');
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error('Update failed!');
            }

        }

        $departmentsList = $departmentsCtx->find('list', ['keyField' => 'id', 'valueField' => 'name'])->toArray();

        $this->set('users', $usersCtx->find('all', ['contain' => 'departments'])->toArray());
        $this->set('departmentsList', $departmentsList);
        $this->set('deleteUrl', Router::url(['controller' => 'auth', 'action' => 'delete'], true));
    }
    
    // TODO: run this on first setup, read username and password from config
    private function setup() {
        $usersCtx = TableRegistry::get('Users');
        $admin = $usersCtx->newEntity(['username' => 'draconigen', 'password' => 'admin', 'role' => 'admin', 'enabled' => true, 'email' => 'draconigen@gmail.com']);
        $usersCtx->save($admin);
    }
}