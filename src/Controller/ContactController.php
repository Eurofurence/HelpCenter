<?php
/**
 * Eurofurence Help Center
 * Contact Controller
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    draconigen@gmail.com
 * @link      https://www.eurofurence.org/help/contact
 * @since     0.3
 * @license   https://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Mailer\Email; // for admin reports


class ContactController extends AppController
{
    public function initialize() {
        parent::initialize();
        $this->Auth->allow();
    }

    public function index($department_reference = null, $template_reference = null) {
        $departmentsCtx = TableRegistry::get('Departments');
        $templatesCtx = TableRegistry::get('Templates');

        $departments = ($departmentsCtx->find('list', ['keyField' => 'id', 'valueField' => 'name']))->toArray();
        $departments = [0 => '(Please choose one)'] + $departments;

        $departmentHints = ($departmentsCtx->find('list', ['keyField' => 'id', 'valueField' => 'hint']))->toArray();
        $departmentHints = [0 => ''] + $departmentHints;

        $templates = $templatesCtx->find('all')->toArray();
        
        foreach($templates as $templ) {
            $departmentTemplateMap[$templ->department_id][] = $templ;
        }

        if (($select = $departmentsCtx->find('all')->where(['reference' => $department_reference])->first()['id']) === null)
            $select = 0;

        if ($this->request->is('get')) {
            if (($template = $templatesCtx->find('all')->where(['department_id' => $select, 'reference' => $template_reference])->first()) !== null) {
                $this->request->data['Subject'] = $template->subject;
                $this->request->data['Message'] = $template->message;
            }
        }

        $this->set('select', $select);
        $this->set('departments', $departments);
        $this->set('departmentHints', $departmentHints);
        $this->set('templates', $departmentTemplateMap);
        $this->set('gSitekey', Configure::read('GoogleReCaptcha.sitekey'));

        /* after "SUBMIT" has been pressed */
        if ($this->request->is('post')) {
            $department = $this->request->data('Departments');
            $name       = $this->request->data('Name');
            $email      = $this->request->data('Mail');
            $subject    = $this->request->data('Subject');
            $message    = $this->request->data('Message');
            $reCaptcha  = $this->request->data('g-recaptcha-response');

            if ($this->verifyInput($department, $name, $email, $subject, $message, $reCaptcha)) {
                $department = $departmentsCtx->get($department, ['fields' => 'email'])['email'];

                // core send
                if (mail($department, $this->prepareSubject($subject), $this->prepareMessageBody($message), $this->prepareHeader($name, $email) )) {
                    $this->Flash->modal('Thank you! We\'ve received your message and will reply as soon as we can. You will receive an automated answer along with a ticket number very shortly.');
                }
                else {
                    $this->Flash->error('An internal error occured! Our website support has been notified. Please try again later or contact us on some other way. Sorry!');
                    Email::deliver(null, '[EFHC] Contact Internal Error', 'Critical error at the EF Help Center Contact form: the core send mail() function has returned false.', 'admin');
                }
            }
            else {
                return;
            }
        }
    }

    private function verifyInput($department, $name, $email, $subject, $message, $reCaptcha) {
        $error = false;

        if ($reCaptcha === null || !$this->verifyReCaptcha($reCaptcha)) {
            $error = true;
            $this->Flash->error('ReCaptcha verification failed. Please try again.');
            // Email::deliver(null, '[EFHC] ReCaptcha Failure', 'An unexpected ReCaptcha Failure has occured at the EF Help Center.', 'admin');
        }

        if (empty($department)) {
            $error = true;
            $this->Flash->error('Please choose a department.');
        }

        if (strlen($name) < 2) {
            $error = true;
            $this->Flash->error('Please enter a name.');
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error = true;
            $this->Flash->error('Please enter your eMail.');
        }

        if (strlen($subject) < 3) {
            $error = true;
            $this->Flash->error('Please enter a subject.');
        }
        
        if (strlen($message) < 9) {
            $error = true;
            $this->Flash->error('Please enter a message.');
        }
        
        return !$error;
    }

    private function verifyReCaptcha($response) {
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret={{secret}}&response={{response}}';
            
        $url = str_replace('{{secret}}', Configure::read('GoogleReCaptcha.secret'), $url);
        $url = str_replace('{{response}}', $response, $url);
        
        $result = file_get_contents($url);
        $resultData = json_decode($result);

        return $resultData->success;
    }
    
    // function adds all given metadata to the actual e-mail body
	private function prepareMessageBody($message) {
		return(
			utf8_decode($message)
		);
	}
	
	// function to return mail header -- mail(..., string headers, ...)
	private function prepareHeader($name, $email) {
		return 
			"MIME-Version: 1.0\r\n" .
			"Content-type: text/plain; charset=\"iso-8859-1\"\r\n" .
			"X-Eurofurence-Source: " . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"] . "\r\n" .
			"From: " . $this->headerEncode($name)." <$email>";
	}
	// function to return mail subject
	private function prepareSubject($subject) {
		return $this->headerEncode($subject);
	}
	
	private function headerEncode($str) {
		$str = utf8_decode($str);
		// $str = str_replace(" ", "_", $str); // only when using quoted_printable_encode() instead of base64_encode()
		$str = "=?ISO-8859-1?B?" . base64_encode($str) . "?=";
		return $str; 
	}
}


